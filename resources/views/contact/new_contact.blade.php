<!doctype html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Contacts</title>

</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">New Contact</div>
                    <div class="card-body">
                        <form method="POST" action="{{ URL::route('registerContact') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="text" class="col-md-4 col-form-label text-md-right">Country Code</label>

                                <div class="col-md-6">
                                    <input id="country_code" type="country_code"
                                        class="form-control @error('country_code') is-invalid @enderror"
                                        name="country_code" value="{{ old('country_code') }}" required
                                        autocomplete="country_code" autofocus>

                                    <input type="hidden" name="people_id" id="people_id" value="{{ $user_id }}">

                                    @error('country_code')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                                <label for="number" class="col-md-4 col-form-label text-md-right">number</label>

                                <div class="col-md-6">
                                    <input id="number" type="number"
                                        class="form-control @error('number') is-invalid @enderror" name="number"
                                        value="{{ old('number') }}" required autocomplete="number" autofocus>

                                    @error('number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <br>
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-danger"
                                        onclick="window.location='{{ URL::route('peoplesHome') }}'">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
