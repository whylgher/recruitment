<?php

namespace App\Http\Controllers;

use App\Http\Requests\PeopleRequest;
use App\Models\People;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PeopleController extends Controller
{
    public function registerr(PeopleRequest $request)
    {
        People::create($request->all());
        return redirect()->route('peoplesHome');
    }

    public function getPeople($id)
    {
        $people = People::find($id);
        return view('people.people', compact('people'));
    }

    public function peoplesHome()
    {
        $user = empty(Auth::user());
        $peoples = People::all();
        return view('people.peoples', compact('peoples', 'user'));
    }

    public function destroy(People $id)
    {
        foreach ($id->contact as $index => $contact) {
            $contact->delete();
        }
        $id->delete();

        return redirect()->route('peoplesHome');
    }

    public function editPeople($id)
    {
        $people = People::find($id);
        return view('people.edit_people', compact('people'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|string|min:5|max:255',
            'email' => 'required|string|email|max:255',
        ];

        $request->validate($rules);
        $people = People::where('id', $id)->update($request->all());
        if ($people) {
            return redirect()->route('getPeople', [$id]);
        }
    }
}
