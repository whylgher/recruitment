<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Models\Contact;
use App\Models\People;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    public function register(ContactRequest $request)
    {
        Contact::create($request->validated());
        return redirect()->route('peoplesHome');
    }

    public function getContact(People $id)
    {
        $user_logged = empty(Auth::user());
        $user = $id;
        return view('contact.contacts', compact('user', 'user_logged'));
    }

    public function contactsHome()
    {
        $user = empty(Auth::user());
        $contacts = Contact::all();
        return view('contact.contacts', compact('contacts', 'user'));
    }

    public function destroy(Contact $id)
    {
        $id->delete();
        return redirect()->route('peoplesHome');
    }

    public function editContact($id)
    {
        $contact = Contact::find($id);
        return view('contact.edit_contact', compact('contact'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'country_code' => 'required|string|min:3|max:5',
            'number' => 'required|min:9|max:9',
        ];
        $contact = Contact::where('id', $id)->update($request->validate($rules));
        if ($contact) {
            return redirect()->route('peoplesHome');
        }
    }
}
