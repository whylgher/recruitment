<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PeopleController;
use App\Http\Middleware\Authenticate;
use Illuminate\Support\Facades\Route;


Route::get('/', [PeopleController::class, 'peoplesHome'])->name('peoplesHome');

Route::get('/new-contact/{user_id}', function ($user_id) {
    return view('contact.new_contact', compact('user_id'));
})->name('newContact');

Route::get('/new-people', function () {
    return view('people.new_people');
})->name('newPeople');

Route::get('/login', function () {
    return view('auth.login');
})->name('loginPage');

Route::get('/edit', function () {
    return view('auth.edit_contact');
})->name('editContactPage');

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login')->name('login');
    Route::post('register', 'register');
    Route::get('logout', 'logout')->name('logout');
    Route::post('refresh', 'refresh');
});

Route::controller(PeopleController::class)->group(function () {
    Route::post('people/register', 'registerr')->name('registerPeople');
    Route::get('people/delete/{id}', 'destroy')->name('deletePeople')->middleware(Authenticate::class);
    Route::get('peoples', 'getAllPeoples');
    Route::get('people/{id}', 'getPeople')->name('getPeople');
    Route::get('people/edit/{id}', 'editPeople')->name('editPeople')->middleware(Authenticate::class);
    Route::get('people/update/{id}', 'update')->name('updatePeople')->middleware(Authenticate::class);
});

Route::controller(ContactController::class)->group(function () {
    Route::post('contact/register', 'register')->name('registerContact');
    Route::get('contact/delete/{id}', 'destroy')->name('deleteContact')->middleware(Authenticate::class);
    Route::get('contacts', 'getAllContacts');
    Route::get('contact/{id}', 'getContact')->name('getContacts');
    Route::get('contact/edit/{id}', 'editContact')->name('editContact')->middleware(Authenticate::class);
    Route::get('contact/update/{id}', 'update')->name('updateContact')->middleware(Authenticate::class);
});
